json.array!(@donations) do |donation|
  json.extract! donation, :id, :amount, :donation_date, :donor_id
  json.url donation_url(donation, format: :json)
end
