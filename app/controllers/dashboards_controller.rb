class DashboardsController < ApplicationController
  before_action :set_dashboard, only: [:show, :edit, :update, :destroy]


  def index
    @donation_total = Donation.total_amt
  end


end
