class Donation < ActiveRecord::Base
  belongs_to :donor

  def self.total_amt
    Donation.sum(:amount)
  end
end
